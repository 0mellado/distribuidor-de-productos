CREATE TABLE IF NOT EXISTS products (
    id_prod INT(10) NOT NULL,
    name_prod VARCHAR(32) NOT NULL,
    type_prod VARCHAR(12) NOT NULL,
    type_weight VARCHAR(12) NOT NULL,
    weight BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (id_prod)
);


CREATE TABLE IF NOT EXISTS vehicles (
    id_vehicle INT(10) NOT NULL,
    name_vehicle VARCHAR(10) NOT NULL,
    price_vehicle INT(32) NOT NULL,
    capacity_vehicle INT(8) NOT NULL,
    PRIMARY KEY (id_vehicle)
);


CREATE TABLE IF NOT EXISTS containers (
    id_cont INT(10) NOT NULL,
    size_cont VARCHAR(10) NOT NULL,
    weight_cont VARCHAR(10) NOT NULL,
    type_cont VARCHAR(12) NOT NULL,
    capacity_cont INT(6) NOT NULL,
    total_weight INT(6) NOT NULL,
    id_vehicle INT(10) NOT NULL,
    FOREIGN KEY (id_vehicle) REFERENCES vehicles (id_vehicle),
    PRIMARY KEY (id_cont)
);

CREATE TABLE IF NOT EXISTS conts_prodcts (
    id_merge INT(16) NOT NULL AUTO_INCREMENT,
    id_cont INT(10) NOT NULL,
    id_prod INT(10) NOT NULL,
    prod_weight INT(10) NOT NULL,
    FOREIGN KEY (id_cont) REFERENCES containers (id_cont),
    FOREIGN KEY (id_prod) REFERENCES products (id_prod),
    PRIMARY KEY (id_merge)
);

