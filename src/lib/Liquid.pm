package Liquid;

use parent 'Container';

use strict;
use warnings;


sub liquid {
    my $self = shift;
    my $weightType = shift;
    $self->{weightType} = $weightType;
    return $self->{weightType};
}


1;
