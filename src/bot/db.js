const mysql = require('mysql')

const conectDb = () => {
    const conection = mysql.createConnection({
        host: process.env.CONN_HOST,
        user: process.env.CONN_USER,
        password: process.env.CONN_PASS,
        database: process.env.CONN_DB
    })

    conection.connect(err => {
        if (err) throw err
    })
    return conection
}


module.exports = conectDb
