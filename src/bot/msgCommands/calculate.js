const {spawn} = require('node:child_process')


module.exports = {
    name: 'calculate',
    async execute(message, args) {
        await message.reply("\`\`\`haciendo los calculos...\`\`\`")

        const truck = args[1] > 0 ? args[1] : '500000'
        const train = args[2] > 0 ? args[2] : '10000000'
        const plane = args[3] > 0 ? args[3] : '1000000'
        const freighter = args[4] > 0 ? args[4] : '1000000000'

        const calcProcess = spawn('perl', ['../main.pl', truck, train, plane, freighter])
        let result = ''

        // message.channel.messages.fetch("1047733870235230338")
        //     .then(message => message.edit("\`\`\`[REDACTED]\`\`\`"))

        calcProcess.stdout.on('data', async data => {
            result = await data.toString()
            console.log(result)
            await message.reply(`\`\`\`${result}\`\`\``)
        })
        calcProcess.stderr.on('data', async () => {
            await message.reply(`\`\`\`Ocurrio algun error en la ejecución del programa\`\`\``)
        })
    }
}
