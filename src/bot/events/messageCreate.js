const fs = require('node:fs')
const path = require('node:path')
const {prefix} = require('../config.json')
const {Client, Events, GatewayIntentBits, Collection} = require('discord.js')

const client = new Client({intents: [GatewayIntentBits.Guilds]});
client.commands = new Collection();

module.exports = {
    name: Events.MessageCreate,
    async execute(message) {
        const commandsPath = path.join(__dirname, '../msgCommands');
        const commandFiles = fs.readdirSync(commandsPath)
            .filter(file => file.endsWith('.js'));

        if (message.author.bot) return;

        if (!message.content.startsWith(prefix)) return;

        const args = message.content.slice(prefix.length).trim().split(/ +/g);

        for (const file of commandFiles) {
            const filePath = path.join(commandsPath, file);
            const command = require(filePath);

            if (!command.name || !command.execute) continue;

            if (command.name == args[0]) await command.execute(message, args);
        }
    }
};
