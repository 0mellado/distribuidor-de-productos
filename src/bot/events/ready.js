const {Events} = require('discord.js')

module.exports = {
    name: Events.ClientReady,
    once: true,
    execute(client) {
        console.log(`El bot esta activo como: ${client.user.tag}`);
    }
};
